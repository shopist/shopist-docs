#Instroduction

Shopist is an application  for managing grocery and shopping lists within family members with live sync.

###Goal

Simplify grocery shopping, money saving, sharing grocery lists with other people.

###Target audience

Anyone who does grocery shopping

###Platforms

(Listed by priority)

1. Web version
   - Chrome
   - Firefox
   - Safari
   - IE 11
   - Microsoft Edge
   - Opera

2. Android version (Android 4.4 and higher)

   - smartphone
   - tablet

3. IOs version
   - iPhone 5s +
   - iPad
   - Apple Watch (?)

4. Chrome extension (?)

5. Windows phone (?)

###Technology stack

#####Front-end

 - React js
 - Material UI
 - Webpack
 - Redux
 - Babel
 - Jed
 - Inline styles
 - JEST
 - Jasmine

####Back-end

 - Node js
 - MongoDB
 - Express
 - Socket.io
 - Passport js
 - Babel
 - Jasmine

#Functional requirements

###Main functionlity

(Divided by parts)

#####Part 1

 - Ssign in and sing up mechanism for new users with email or social networks (google, facebook, vk)
 - Creation of shopping lists
 - Adding products to existing lists
 - Check products off the list
 - Add some starting base of products

#####Part 2

 - Sharing lists with other people by e-mail
 - Live sync in shopping lists
 - Push notifications
 - Creating pantry lists

#####Part 3

 - Localization (english, russian, ukrainian languages)
 - Extended products description (add notes, comments, unit price)
 - Categorizing items (using existing categories, adding new categories)
 - Add places to buy items (e.g. supermarkets, molls, stores, markes and assign items to them)

#####Part 4

 - Add recipes. Recipe should contain name, description, steps and required products. When user selects recipe, all items are automatically added to his shopping list

#####Part 5

 - Interface customization (selecting themes)
 - Android version with PhoneGap

###Geo-location services and push notifications

Geo-location recognition for notifyin of markets nearby.

###Social media integration

 - Facebook
 - Google
 - VK
 - Twitter
 - Linkedin
 - Git

#Design

Material design from google

#Deadlines

_Part 1_ - 10/31/2015

_Part 2_ - 12/10/2015

_Part 3_ - 02/01/2016

_Part 4_ - 03/03/2016

_Part 5_ - 10/04/2016
